function imgOut = ChiuTMO(img, c_sigma)

check13Color(img);
checkNegative(img);


L = lum(img);
r = size(img, 1);
c = size(img, 2);
c_k = 8;
c_clamping = 500;

if(~exist('c_sigma', 'var'))
    c_sigma = round(16 * max([r, c]) / 1024) + 1;
end

%calculate s
s = RemoveSpecials(1 ./ (c_k * filterGaussian(L, c_sigma)));

if(c_clamping > 0) %clamp s
    L_inv = RemoveSpecials(1 ./ L);
    indx = find(s >= L_inv);
    s(indx) = L_inv(indx);

    % smoothe s
    H = [0.080 0.113 0.080;...
         0.113 0.227 0.113;...
         0.080 0.113 0.080];

    for i=1:c_clamping
        s = imfilter(s, H, 'replicate');
    end
end

glare_opt(1) = 0.8;
glare_opt(2) = 8;    
glare_opt(3) = 121;

% tone mapping
Ld = ChiuGlare(L .* s, glare_opt);

% change luminance
imgOut = ChangeLuminance(img, L, Ld);

end