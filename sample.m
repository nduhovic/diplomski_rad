
addpath('\util');

img = hdrimread('memorial.hdr');

h = figure(1);
set(h,'Name','HDR vizualizacija');
GammaTMO(img, 1.0, 0, 1);

h = figure(2);
set(h,'Name','Logaritamski');
imgTMO= LogarithmicTMO(img);
imgOut = GammaTMO(imgTMO, 2.2, 0, 1);
%imwrite(imgOut, '/output/logaritamski.png');

h = figure(3);
set(h,'Name','Schlick');
imgTMO= SchlickTMO(img);
imgOut = GammaTMO(imgTMO, 2.2, 0, 1);
%imwrite(imgOut, '/output/schlick.png');

h = figure(4);
set(h,'Name','chiu1');
imgOut = GammaTMO(ChiuTMO(img), 2.2, 0, 1);
%imwrite(imgOut, '/output/chiu.png');


h = figure(5);
set(h,'Name','Reinhard');
imgTMO = ReinhardTMO(img,0.1, 100);
imgOut = GammaTMO(imgTMO, 2.2, 0, 1);
%imwrite(imgOut, '/output/reinhard.png');

