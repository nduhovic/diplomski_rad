addpath('\util');
path = '\images';
files = dir (strcat(path,'\*.hdr'));
L = length (files);

fid=fopen('\analysis.txt','w');
for i=1:L
    disp(i);
    img = hdrimread(strcat(path,'\',files(i).name));

    %logaritamski
    tic;
    imgTMO= LogarithmicTMO(img);
    time=toc;
    fprintf(fid, '%f, ', time);

    %schlick
    tic;
    imgTMO= SchlickTMO(img);
    time=toc;
    fprintf(fid, '%f, ', time);
    
    %chiu
    tic;
    imgTMO= ChiuTMO(img);
    time=toc;
    fprintf(fid, '%f, ', time);
    
    %reinhard
    tic;
    imgTMO = ReinhardTMO(img);
    time=toc;
    fprintf(fid, '%f \r\n', time);
end

fclose(fid);