function [imgOut, pAlpha, pWhite] = ReinhardTMO(img, pAlpha, pWhite)

check13Color(img);

% get luminance
L = lum(img);


if(~exist('pAlpha', 'var'))
    pAlpha = 0.2;
end

if(~exist('pWhite', 'var'))
    pWhite = 100;
end


% compute logarithmic mean
Lwa = logMean(L);        

% scale luminance using alpha and logarithmic mean
Lm = (pAlpha * L) / Lwa;

% compute adaptation
L_adapt = ReinhardFiltering(Lm, pAlpha);

% range compression
Ld = (Lm .* (1 + Lm / pWhite^2)) ./ (1 + L_adapt);

% change luminance
imgOut = ChangeLuminance(img, L, Ld);

end