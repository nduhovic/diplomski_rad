function imgOut = SchlickTMO(img, nBit, L0)    

check13Color(img);
check3Color(img);

if(~exist('nBit', 'var'))
    nBit = 8;
end

if(~exist('L0',  'var'))
    L0 = 1;
end

L = lum(img); % get luminance channel

LMax = max(L(:)); % compute the max luminance

LMin = min(L(:));  % compute min luminance
if (LMin<0.01)
    LMin=0.01;
end

p = L0 * LMax / (2^nBit * LMin);

% reduction of dynamic range
Ld = p .* L ./ ((p - 1) .* L + LMax);

% change luminance
imgOut = ChangeLuminance(img, L, Ld);

end
