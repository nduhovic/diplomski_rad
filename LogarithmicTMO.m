function imgOut = LogarithmicTMO(img, log_q, log_k)

check13Color(img);
checkNegative(img);

if(~exist('log_q', 'var'))
    log_q = 1;
end

if(~exist('log_k', 'var'))
    log_k = 1;
end

%get luminance
L = lum(img);
%compute the max luminance
LMax = max(L(:)); 

%dynamic range reduction
Ld = log10(1 + L * log_q) / log10(1 + LMax * log_k);

%change luminance in img
imgOut = ChangeLuminance(img, L, Ld);

end